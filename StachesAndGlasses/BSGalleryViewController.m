//
//  BSGalleryViewController.m
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/5/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSGalleryViewController.h"
#import <Parse/Parse.h>

@interface BSGalleryViewController ()
@property (strong, nonatomic) NSMutableArray *photos;
@end

@implementation BSGalleryViewController


-(void)viewWillAppear:(BOOL)animated{
    _photos = [[NSMutableArray alloc]init];
    PFQuery *query = [PFQuery queryWithClassName:@"UserPhoto"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        for (PFObject *object in objects) {
            PFFile *imageFile = [object objectForKey:@"imageFile"];
            [imageFile getDataInBackgroundWithBlock:^(NSData *result, NSError *error) {
                if (!error && result) {
                    UIImage *image = [UIImage imageWithData:result];
                    [_photos addObject:image];
                    [self.collectionView reloadData];
                }
            }];
        }
    }];
}

#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    //    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    //    return [sectionInfo numberOfObjects];
    return [_photos count];
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[_photos objectAtIndex:indexPath.row]];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    imageView.frame = CGRectMake(0, 0,screenWidth/2,screenWidth/2);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell sizeToFit];
    
    [cell addSubview:imageView];
    
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
