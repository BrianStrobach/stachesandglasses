//
//  BSFilterButton.m
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/4/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSFilterButton.h"

@implementation BSFilterButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
