//
//  BSPhotoEditingCollectionViewController.h
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/4/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MBProgressHUD.h"
#import <Parse/Parse.h>
#import "PaintView.h"

@interface BSPhotoEditingViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, MBProgressHUDDelegate, PaintViewDelegate>

@end
