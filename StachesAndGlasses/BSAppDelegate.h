//
//  BSAppDelegate.h
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/4/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
