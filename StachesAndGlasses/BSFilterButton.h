//
//  BSFilterButton.h
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/4/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSFilterButton : UIButton
@property (strong, nonatomic) CIFilter *filter;
@end
