//
//  BSGalleryViewController.h
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/5/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSGalleryViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
