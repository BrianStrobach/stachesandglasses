//
//  BSPhotoEditingCollectionViewController.m
//  StachesAndGlasses
//
//  Created by Brian Strobach on 5/4/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSPhotoEditingViewController.h"
#import "BSFilterButton.h"


@interface BSPhotoEditingViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *mainPhotoImageView;
@property (strong, nonatomic) IBOutlet UIButton *importPhotoButton;
@property (strong, nonatomic) IBOutlet UIScrollView *filterScrollView;
@property (strong, nonatomic) IBOutlet UIButton *drawButton;
@property (strong, nonatomic) NSMutableArray *filterButtons;
@property BOOL newMedia;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) MBProgressHUD *refreshHUD;
@property (strong, nonatomic) PaintView *paintView;
@property (strong, nonatomic) NSMutableArray *localImageCache;
@property (strong, nonatomic) UIWindow *window;
@property BOOL drawingEnabled;

@end

@implementation BSPhotoEditingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _filterButtons = [[NSMutableArray alloc]init];
    _drawingEnabled = NO;
    
    _localImageCache = [[NSMutableArray alloc] init];
    
    
}

- (IBAction)saveButtonPressed:(UIBarButtonItem *)sender {
    if (!_mainPhotoImageView.image) {
        [self showAlert];
        return;
        
    }
    NSData *imageData = UIImageJPEGRepresentation(_mainPhotoImageView.image, 0.05f);
    [self uploadImage:imageData];
}

- (IBAction)addPhotoButtonPressed:(id)sender {
    [self useCamera];
}
- (IBAction)drawButtonPressed:(id)sender {
    if (!_mainPhotoImageView.image) {
        [self showAlert];
        return;
        
    }
    [self enableDrawingMode];
}
- (IBAction)importButtonPressed:(id)sender {
    [self useCameraRoll];
    
}

-(void)enableDrawingMode{
    if(_mainPhotoImageView.image){
        _drawingEnabled = !_drawingEnabled;
        if (_drawingEnabled ) {
            [self.drawButton setTitle:@"Done" forState:UIControlStateNormal];
            
            _paintView = [[PaintView alloc]initWithFrame:self.mainPhotoImageView.frame];
            self.paintView.lineColor = [UIColor blueColor];
            self.paintView.delegate = self;
            self.paintView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            [self.view addSubview:self.paintView];
        }
        else{
            [self.drawButton setTitle:@"Draw" forState:UIControlStateNormal];
            [_paintView removeFromSuperview];

        }
    }
    else{
        NSLog(@"Must first add image to draw");
    }
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self.localImageCache removeAllObjects];
}

///-----------------------------------------------------------------------------
#pragma mark - Shake To Erase
///-----------------------------------------------------------------------------
- (BOOL)canBecomeFirstResponder
{
    return YES;
}

// Erase on shake
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventTypeMotion && event.type == UIEventSubtypeMotionShake) {
        self.mainPhotoImageView.image = nil;
    }
}

///-----------------------------------------------------------------------------
#pragma mark - PaintView Delegate Methods
///-----------------------------------------------------------------------------
- (void)paintView:(PaintView*)paintView finishedTrackingPath:(CGPathRef)path inRect:(CGRect)painted
{
    [self mergePaintToBackgroundView:painted];
}

- (void)mergePaintToBackgroundView:(CGRect)painted
{
    NSLog(@"Merging Paint");
    
    // Create a new offscreen buffer that will be the UIImageView's image
    CGRect bounds = self.mainPhotoImageView.bounds;
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, self.mainPhotoImageView.contentScaleFactor);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Copy the previous background into that buffer.  Calling CALayer's renderInContext: will redraw the view if necessary
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    [self.mainPhotoImageView.layer renderInContext:context];
    
    // Now copy the painted contect from the paint view into our background image
    // and clear the paint view.  as an optimization we set the clip area so that
    //we only copy the area of paint view that was actually painted
    CGContextClipToRect(context, painted);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [self.paintView.layer renderInContext:context];
    [self.paintView erase];
    
    // Create UIImage from the context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    self.mainPhotoImageView.image = image;
    UIGraphicsEndImageContext();
    
}
-(void)useCamera{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }
    _newMedia = YES;
    
}

-(void)useCameraRoll{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
        _newMedia = NO;
    }
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image;
        if (_newMedia) {
            image = info[UIImagePickerControllerEditedImage];
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
        }
        else{
            image = info[UIImagePickerControllerOriginalImage];
            CGRect cropRect = CGRectMake(0, 0, image.size.width, image.size.width);
            CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
            // or use the UIImage wherever you like
            image = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            
        }
        _mainPhotoImageView.image = image;
        
        [self buildFilterButtonsForImage:image];
        
    }
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)buildFilterButtonsForImage:(UIImage *)image{
    NSArray* filters = [CIFilter filterNamesInCategories:@[kCICategoryColorEffect]];
    NSLog(@"%i", [filters count]);
    [_filterButtons removeAllObjects];
    for (int i = 0; i < 8; i++) {
        NSString *filterName = [filters objectAtIndex:i + 12];
        NSLog(@"Filter: %@", filterName);
        NSLog(@"Parameters: %@", [[CIFilter filterWithName:filterName] attributes]);
        BSFilterButton *newButton = [[BSFilterButton alloc]init];
        
        CIImage *unfilteredImage = [[CIImage alloc]initWithImage:image];
        newButton.filter = [CIFilter filterWithName:filterName keysAndValues:@"inputImage", unfilteredImage, nil];
        
        CIImage *filteredImage = [newButton.filter outputImage];
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgimg = [context createCGImage:filteredImage fromRect:[filteredImage extent]];
        UIImage *newImage = [UIImage imageWithCGImage:cgimg];
        
        [newButton setBackgroundImage:newImage
                             forState:UIControlStateNormal];
        double imageWidth = _filterScrollView.frame.size.height - 5;
        [newButton setFrame:CGRectMake(i * (imageWidth + 5), 0, imageWidth,imageWidth)];
        [_filterButtons addObject:newButton];
        [_filterScrollView addSubview:newButton];
        CGImageRelease(cgimg);
        
        [newButton addTarget:self
                      action:@selector(filterButtonPressed:)
            forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    
    _filterScrollView.contentSize = CGSizeMake([_filterButtons count] * _filterScrollView.frame.size.height, _filterScrollView.frame.size.height);
    
}

-(IBAction)filterButtonPressed:(BSFilterButton *)sender{
    if (!_mainPhotoImageView.image) {
        [self showAlert];
        return;
        
    }
    UIImage *image = [sender backgroundImageForState:sender.state];
    [_mainPhotoImageView setImage:image];
}
- (IBAction)shareButtonPressed:(UIBarButtonItem *)sender {
    
    if (!_mainPhotoImageView.image) {
        [self showAlert];
        return;

    }
  
        
        NSString *postString = [NSString stringWithFormat:@"Suck it instagram!"];
        NSArray *activityItems = [NSArray arrayWithObjects:postString,_mainPhotoImageView.image, nil];
        
        
        UIActivityViewController *activityController =
        [[UIActivityViewController alloc]
         initWithActivityItems:activityItems
         applicationActivities:nil];
        
        activityController.completionHandler = ^(NSString *activityType, BOOL completed){
            NSLog(@"Activity Type selected: %@", activityType);
            if (completed) {
                NSLog(@"Selected activity was performed.");
            } else {
                if (activityType == NULL) {
                    NSLog(@"User dismissed the view controller without making a selection.");
                } else {
                    NSLog(@"Activity was not performed.");
                }
            }
        };
        
        [self presentViewController:activityController
                           animated:YES
                         completion:nil];
    
}
- (IBAction)addGlassesAndStaches:(id)sender {
    if (!_mainPhotoImageView.image) {
        [self showAlert];
        return;
        
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CIImage *image = [[CIImage alloc] initWithImage:self.mainPhotoImageView.image];
        
        NSDictionary *options = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:options];
        
        NSArray *features = [detector featuresInImage:image];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self drawImage:self.mainPhotoImageView.image AnnotatedWithFeatures:features];
        });
        
    });
}

- (void)drawImage:(UIImage *)image AnnotatedWithFeatures:(NSArray *)features {
    
	UIImage *faceImage = image;
    UIGraphicsBeginImageContextWithOptions(self.mainPhotoImageView.bounds.size, YES, 0);
    [faceImage drawInRect:self.mainPhotoImageView.bounds];
    
    // Get image context reference
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Flip Context
    CGContextTranslateCTM(context, 0, self.mainPhotoImageView.bounds.size.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    if (scale > 1.0) {
        // Loaded 2x image, scale context to 50%
        CGContextScaleCTM(context, 0.5, 0.5);
    }
    
    for (CIFaceFeature *feature in features) {
        
        
        
        // Set red feature color
        CGContextSetRGBFillColor(context, 1.0f, 0.0f, 0.0f, 0.4f);
        
        if (feature.hasLeftEyePosition && feature.hasRightEyePosition) {
            UIImage *glasses = [UIImage imageNamed:@"glasses"];
            glasses = [UIImage imageWithCGImage:glasses.CGImage
                                          scale:glasses.scale orientation: UIImageOrientationDown];
            
            CGPoint position = CGPointMake((feature.leftEyePosition.x + feature.rightEyePosition.x) / 2, (feature.leftEyePosition.y + feature.rightEyePosition.y)/2);
            
            [glasses drawAtPoint:CGPointMake(position.x - glasses.size.width *.8, position.y - glasses.size.height * 2)];
        }
        
        
        if (feature.hasMouthPosition) {
            //            [self drawFeatureInContext:context atPoint:feature.mouthPosition];
            UIImage *mustache = [UIImage imageNamed:@"mustache"];
            mustache = [UIImage imageWithCGImage:mustache.CGImage
                                           scale:mustache.scale orientation: UIImageOrientationDown];
            [mustache drawAtPoint:CGPointMake(feature.mouthPosition.x - mustache.size.width *.9, feature.mouthPosition.y - mustache.size.height/2)];
            
        }
    }
    
    self.mainPhotoImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
}

- (void)drawFeatureInContext:(CGContextRef)context atPoint:(CGPoint)featurePoint {
    CGFloat radius = 20.0f * [UIScreen mainScreen].scale;
    CGContextAddArc(context, featurePoint.x - radius * 2, featurePoint.y - radius * 2, radius, 0, M_PI * 2, 1);
    CGContextDrawPath(context, kCGPathFillStroke);
}



- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD hides
    [_HUD removeFromSuperview];
    _HUD = nil;
}

-(void)uploadImage:(NSData *)imageData{
    PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
    
    _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Save PFFile
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hide old HUD
            [_HUD hide:YES];
            // Create a PFObject around a PFFile and associate it with the current user
            PFObject *userPhoto = [PFObject objectWithClassName:@"UserPhoto"];
            [userPhoto setObject:imageFile forKey:@"imageFile"];
            
            [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error){
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        }
        else{
            [_HUD hide:YES];
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    } progressBlock:^(int percentDone) {
        // Update your progress spinner here. percentDone will be between 0 and 100.
        _HUD.progress = (float)percentDone/100;
    }];
}

-(void)showAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"No image"
                                                 message:@"You must first select an image."
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
    [av show];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
